// AUTHORIZATION AS CHILDREN
import React, { Component } from 'react';
import './App.css';
import Dashboard from './Dashboard';
import Authorization from './Authorization';

const allowed = ['manager', 'admin'];

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        name: 'Bolek z TSH',
        role: 'adminx'
      }
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">TSH test</h1>
        </header>
        <Dashboard />
        <Authorization allowedRoles={allowed} user={this.state.user}>
          <h1>Mam dostep!</h1>
        </Authorization>
      </div>
    );
  }
}

export default App;


//// AUTHORIZATION.js
import React from 'react';

export default class Authorization extends React.PureComponent {
  render() {
    const { allowedRoles, children, user } = this.props;

    if (allowedRoles.includes(user.role)) {
      return children;
    } else {
      return <h1>No access for this page!</h1>
    }
  }
}

//====================================================================================
// WITH LOADER


import React from 'react';
import withAuthorizaton from './HOCs/withAuthorization';
import withLoader from './HOCs/withLoader';

const Dashboard = () => (
  <h1>Dashboard</h1>
);

const authorizationDashboard = withAuthorizaton(Dashboard, ['manager', 'admin']);

export default withLoader(authorizationDashboard);




