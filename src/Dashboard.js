import React from 'react';
import withAuthorizaton from './HOCs/withAuthorization';

const Dashboard = () => (
  <h1>Dashboard</h1>
);

const authorizationDashboard = withAuthorizaton(Dashboard, ['manager', 'admin']);

export default authorizationDashboard;
