import React from 'react';

export default function withAuthorization(WrappedComponent, allowedRoles) {
  return class extends React.PureComponent {
    constructor(props) {
      super(props);

      this.state = {
        user: {
          name: 'Bolek z TSH',
          role: 'admin'
        }
      }
    }

    render() {
      const { role } = this.state.user;

      if (allowedRoles.includes(role)) {
        return <WrappedComponent {...this.props} />
      } else {
        return <h1>No access for this page!</h1>
      }
    }
  }
}
